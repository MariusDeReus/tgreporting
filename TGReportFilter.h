//
//  TGReportFilter.h
//  OverviewX
//
//  Created by Marius de Reus on 20/11/14.
//  Copyright (c) 2014 teamGiants. All rights reserved.
//

/* A report can contain predifined filters to reduce the number of reports needed.
 *
 * There are several types of filters:
 * - Date filters
 * - Related unique ids, commonly used in relations
 * - A fixed list of possible values
 * - A query is provided to get possible values from the database
 *
 * Instantiate this class by initWithXMLNode
 *
 * possibleValues will return the values to be proposed to the user
 *
 *
 */


#import <Foundation/Foundation.h>

typedef NS_ENUM (NSUInteger, TGReportFilterType)
{
    TGReportFilterTypeUnknown = 0,
    TGReportFilterTypeDate,             // A date should be provided
    TGReportFilterTypeString,           // a string should be provided
    TGReportFilterTypeArray,            // One or more of the provided values should be used
    TGReportFilterTypeQuery             // a query is provided to get the possible values
};

@interface TGReportFilter : NSObject

@property (readonly) TGReportFilterType type;
@property (readonly) NSString *name;
@property (readonly) NSString *filterDescription;
@property (readonly) NSArray *possibleValues; // Array of dictionaries with possible title / id combinations

@property (strong) id filterValue;

- (instancetype)initWithXMLNode:(NSXMLNode *)aNode;

@end
