//
//  PrintWindowWindowController.h
//  kantoorXplorer
//
//  Created by Marius de Reus on 02-01-13.
//  Copyright (c) 2013 teamGiants. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TGReportWindowController : NSWindowController <NSOutlineViewDataSource, NSOutlineViewDelegate>

- (instancetype)initWithReportGroups:(NSArray *)aGroups variables:(NSDictionary *)aVariables;

@end
