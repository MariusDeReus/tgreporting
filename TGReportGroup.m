//
//  TGReportGroup.m
//  OverviewX
//
//  Created by Marius de Reus on 16/03/15.
//  Copyright (c) 2015 teamGiants. All rights reserved.
//


#import "TGReportGroup.h"
#import "TGReporting.h"
#import "TGReport.h"

// === Application specific imports ===
#import "TGAppDelegate.h"
#import "TGUserDefaults.h"
#import "TGSettings.h"
// ===

@interface TGReportGroup ()

@property NSString *relativePath;
@property (readwrite) NSString *name;

@property (readwrite, nonatomic) NSArray *reports;

@end

@implementation TGReportGroup

- (instancetype)initWithRelativePath:(NSString *)aPath name:(NSString *)aName
{
    self = [super init];
    if (self)
    {
        _relativePath = aPath;
        _name = aName;
    }
    return self;
}

- (NSArray *)reports
{
    if (!_reports)
    {
        NSError *lError;
        
        // Append path to reportMainPath
        NSURL *lMainFolderURL = [[self class] reportsMainURLError:&lError];
        NSURL *lFolderURL = [lMainFolderURL URLByAppendingPathComponent:self.relativePath];
        
        if (lFolderURL)
        {
            NSMutableArray *lReportsInFolder = [NSMutableArray new];
            NSArray *lReportURLs = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:lFolderURL
                                                                 includingPropertiesForKeys:@[NSURLNameKey]
                                                                                    options:NSDirectoryEnumerationSkipsHiddenFiles|NSDirectoryEnumerationSkipsSubdirectoryDescendants
                                                                                      error:&lError];
            // Iterate reportfiles in this folder
            for (NSURL *lReportURL in lReportURLs)
            {
                NSNumber *isDirectory;
                [lReportURL getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:NULL];
                NSString *lFileName = [lReportURL lastPathComponent];
                
                // Skip directories and names starting with underscore (are subreports)
                if ([isDirectory boolValue] == NO && ![lFileName hasPrefix:@"_"])
                {
                    TGReport *lReport = [[TGReport alloc] initWithReportURL:lReportURL];
                    [lReportsInFolder addObject:lReport];
                }
            }
            _reports = [lReportsInFolder copy];
        }
        else
        {
            NSLog(@"%@", lError);
        }
    }
    return _reports;
}

#pragma mark - Framework, customizable class methods

+ (NSURL*)reportsMainURLError:(NSError **)outError
{
    NSURL *rv;
    NSString *lErrorDescription;
    NSInteger lErrorCode = 0;
    TGAppDelegate *lAppDelegate = [TGAppDelegate appDelegate];
    
    // Override report main path by runtime params when available
    NSString *lReportsMainPath = [TGUserDefaults sharedDefaults].reportMainPath;
    if (!lReportsMainPath)
    {
        lReportsMainPath = lAppDelegate.currentSettings.settingsReportMainPath;
    }
    
    if (lReportsMainPath)
    {
        // Ok, we have a main path for the reports
        NSURL *lMainURL = [NSURL fileURLWithPath:lReportsMainPath];
        if ([[NSFileManager defaultManager] fileExistsAtPath:lMainURL.path])
        {
            rv = lMainURL;
        }
        else
        {
            lErrorCode = cReportErrorMainFolderNotFound;
            lErrorDescription = [NSString stringWithFormat:NSLocalizedString(@"Er zijn geen rapporten aanwezig (pad: '%@')", @"error"), lReportsMainPath];
        }
    }
    else
    {
        lErrorCode = cReportErrorMainFolderNotDefined;
        lErrorDescription = [NSString stringWithFormat:NSLocalizedString(@"Er is geen rapporten map gedefinieerd in de gebruikersinstellingen", @"error")];
    }
    
    // If error occurred
    if (lErrorCode != 0 && outError)
    {
        // Create and return the custom domain error.
        NSDictionary *errorDictionary = @{ NSLocalizedDescriptionKey : lErrorDescription};
        *outError = [[NSError alloc] initWithDomain:cReportErrorDomain
                                               code:lErrorCode userInfo:errorDictionary];
    }
    return rv;
}

@end
