//
//  TGReport+Parsing.h
//  OverviewX
//
//  Created by Marius de Reus on 05/03/15.
//  Copyright (c) 2015 teamGiants. All rights reserved.
//

#import "TGReport.h"

@interface TGReport (Parsing)


@property (readwrite) NSNumber *progress;
@property BOOL cancelledByUser;

// Returns the parsed report or nil
- (NSString *)parsedHTMLWithVariables:(NSDictionary *)aVariables error:(NSError **)outError;

// Cancel the parsing process
- (void)onCancelParsing;

// Applications specific implementation of querying data
+ (NSArray *)dataForQuery:(NSString *)aQuery;


@end
