//
//  TGReport.m
//  OverviewX
//
//  Created by Marius de Reus on 02/03/15.
//  Copyright (c) 2015 teamGiants. All rights reserved.
//

#import "TGReport.h"
#import "TGReportFilter.h"

// A4 = 595  x 842 points. 1 point is 0.35 mm
CGFloat const cMillimeterToPoints = 1/0.35;
CGFloat const cInvalidValue = -1;
NSString * const cMetaNodeXPath = @"/html/head/meta";
NSString * const cReportFiltersNodeXPath = @"/html/head/filter";

@interface TGReport ()

@property NSURL *reportURL;
@property (readwrite, nonatomic) NSArray *filters;

@property (strong, nonatomic) NSXMLDocument *baseReport;
@property NSArray *metaNodes;

// PrintInfo properties
@property (readonly) NSPaperOrientation orientation; // Intended orientation
@property (readonly) CGFloat leftMargin;
@property (readonly) CGFloat rightMargin;
@property (readonly) CGFloat topMargin;
@property (readonly) CGFloat bottomMargin;

@end


@implementation TGReport

- (instancetype)initWithReportURL:(NSURL*)aReportURL
{
    // Designated initializer
    self = [super init];
    
    if (self)
    {
        _reportURL = aReportURL;
    }
    return self;
}

#pragma mark - Getters

- (NSString *)name
{
    // Our URL without extension
    return self.reportURL.lastPathComponent;
}

- (NSXMLDocument*)baseReport
{
    // Step 1: Load the base report as XMLDocument
    if (!_baseReport)
    {
        NSError *lError;
        _baseReport = [[NSXMLDocument alloc] initWithContentsOfURL:self.reportURL options:0 error:&lError];
        if (!_baseReport)
        {
            NSLog(@"Error loading: %@", lError);
        }
    }
    return _baseReport;
}

- (NSArray *)metaNodes:(NSError **)outError
{
    if (!_metaNodes)
    {
        NSError *lError;
        //array of reportParameters
        _metaNodes = [self.baseReport nodesForXPath:cMetaNodeXPath error:&lError];
        if (!_metaNodes)
        {
            NSLog(@"Error loading meta nodes: %@", lError);
        }
    }
    return _metaNodes;
}

- (NSString *)reportDescription
{
    return [self contentValueForMetaNodeWithName:@"description"];
}

- (NSArray *)filters
{
    if (!_filters)
    {
        NSError *lError;
        NSArray *lFilterElements = [self.baseReport nodesForXPath:cReportFiltersNodeXPath error:&lError];
        
        NSMutableArray *lFilters = [[NSMutableArray alloc] initWithCapacity:[lFilters count]];
        for (NSXMLElement *lFilterElement in lFilterElements)
        {
            TGReportFilter *lFilter = [[TGReportFilter alloc] initWithXMLNode:lFilterElement];
            [lFilters addObject:lFilter];
        }
        _filters = [lFilters copy];
    }
    return _filters;
}

#pragma mark - PrintInfo getters

- (NSPrintInfo *)printInfo
{
    NSPrintInfo *rv = [[NSPrintInfo alloc] init];
    rv.orientation = self.orientation;
    rv.leftMargin = self.leftMargin;
    rv.rightMargin = self.rightMargin;
    rv.topMargin = self.topMargin;
    rv.bottomMargin = self.bottomMargin;
    
    return rv;
}

- (NSPaperOrientation)orientation
{
    NSPaperOrientation rv = NSPaperOrientationPortrait; // Default
    NSString *lOrientation = [self contentValueForMetaNodeWithName:@"orientation"];
    
    if ([lOrientation isEqualToString:@"landscape"])
    {
        rv = NSPaperOrientationLandscape;
    }
    return rv;
}

- (CGFloat)leftMargin
{
    CGFloat rv = cInvalidValue;
    id lValue = [self contentValueForMetaNodeWithName:@"leftMargin"];
    if (lValue)
    {
        rv = [lValue doubleValue];
    }
    if (rv == cInvalidValue)
    {
        rv = 25.0 * cMillimeterToPoints; // Fall back to reasonable default
    }
    return rv;
}

- (CGFloat)rightMargin
{
    CGFloat rv = cInvalidValue;
    id lValue = [self contentValueForMetaNodeWithName:@"rightMargin"];
    if (lValue)
    {
        rv = [lValue doubleValue];
    }
    if (rv == cInvalidValue)
    {
        rv = 18.0 * cMillimeterToPoints; // Fall back to reasonable default
    }
    return rv;
}

- (CGFloat)topMargin
{
    CGFloat rv = cInvalidValue;
    id lValue = [self contentValueForMetaNodeWithName:@"topMargin"];
    if (lValue)
    {
        rv = [lValue doubleValue];
    }
    if (rv == cInvalidValue)
    {
        rv = 20 * cMillimeterToPoints; // Fall back to reasonable default
    }
    return rv;
}

- (CGFloat)bottomMargin
{
    CGFloat rv = cInvalidValue;
    id lValue = [self contentValueForMetaNodeWithName:@"bottomMargin"];
    if (lValue)
    {
        rv = [lValue doubleValue];
    }
    if (rv == cInvalidValue)
    {
        rv = 20 * cMillimeterToPoints; // Fall back to reasonable default
    }
    return rv;
}

- (NSString *)contentValueForMetaNodeWithName:(NSString *)aName
{
    NSString *rv;
    NSError *lError;
    NSString *lXPathQuery = [NSString stringWithFormat:@"/html/head/meta[@name='%@']", aName];
    NSArray *lNodes = [self.baseReport nodesForXPath:lXPathQuery error:&lError];
    if ([lNodes count] > 0)
    {
        NSXMLElement *lElement = lNodes.firstObject; // Should be only one
        
        if (lElement)
        {
            rv = (NSString *)[[lElement attributeForName:@"content"] objectValue];
        }
    }
    return rv;
}

@end
