//
//  TGReportFilter.m
//  OverviewX
//
//  Created by Marius de Reus on 20/11/14.
//  Copyright (c) 2014 teamGiants. All rights reserved.
//

#import "TGReportFilter.h"

#import "TGReport+Parsing.h"

@interface TGReportFilter ()

@property (strong) NSXMLElement *XMLNode;

@end


@implementation TGReportFilter

- (instancetype)initWithXMLNode:(NSXMLElement *)aNode
{
    self = [super init];
    if (self)
    {
        _XMLNode = aNode;
        _filterValue = [aNode attributeForName:@"default"].stringValue; // Set only once during init
    }
    return self;
}

- (TGReportFilterType)reportType
{
    NSString *lTypeString = [self.XMLNode attributeForName:@"type"].stringValue;
    if ([lTypeString isEqualToString:@"date"])
    {
        return TGReportFilterTypeDate;
    }
    else if ([lTypeString isEqualToString:@"query"])
    {
        return TGReportFilterTypeQuery;
    }
    else if ([lTypeString isEqualToString:@"string"])
    {
        return TGReportFilterTypeString;
    }
    else if ([lTypeString isEqualToString:@"array"])
    {
        return TGReportFilterTypeArray;
    }
    return TGReportFilterTypeUnknown;
}

- (NSString *)name
{
    return [self.XMLNode attributeForName:@"name"].stringValue;
}

- (NSString *)filterDescription
{
    return [self.XMLNode attributeForName:@"description"].stringValue;
}

- (NSArray *)possibleValues
{
    NSArray *rv;
    if (self.type == TGReportFilterTypeQuery)
    {
        NSString *lQuery = [self.XMLNode attributeForName:@"query"].stringValue;
        NSArray *lDataArray = [TGReport dataForQuery:lQuery];
        if ([lDataArray count] == 0)
        {
            NSLog(@"No rows in query %@",lQuery);
        }
        else
        {
            // Resulting records should have 'title' and 'id' fields
            NSIndexSet *lSet = [lDataArray indexesOfObjectsPassingTest:^BOOL(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
                return (obj[@"id"] && obj[@"title"]);
            }];
            
            rv = [lDataArray objectsAtIndexes:lSet];
        }
    }
    else if (self.type == TGReportFilterTypeArray)
    {
        NSString *lCSVString = [self.XMLNode attributeForName:@"possibleValues"].stringValue;
        rv = [lCSVString componentsSeparatedByString:@","];
    }
    return rv;
}


@end
