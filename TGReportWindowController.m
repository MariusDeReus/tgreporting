//
//  PrintWindowWindowController.m
//  kantoorXplorer
//
//  Created by Marius de Reus on 02-01-13.
//  Copyright (c) 2013 teamGiants. All rights reserved.
//

#import "TGReportWindowController.h"

#import <WebKit/WebKit.h>
#import <Quartz/Quartz.h>

#import "TGReportGroup.h"
#import "TGReport.h"
#import "TGReport+Parsing.h"
#import "TGReportFilter.h"

// Application specific
#import "TGAppDelegate.h"
#import "TGUser.h"
#import "TGSettings.h"
#import "TGUserDefaults.h"


@interface TGReportWindowController ()

@property NSArray *reportGroups;                        // Array with TGReportGroup objects
@property NSDictionary *variables;                      // Values for use in report
@property (strong, nonatomic) TGReport *currentReport;  // The currently selected report
@property NSPrintInfo *printInfo;
@property WebView *reportWebview;                       // Offscreen webview to render the report
@property BOOL enablePrint;                             // Enables/disables print button

// GUI
@property IBOutlet NSProgressIndicator *progressBar;
@property (weak) IBOutlet NSOutlineView *outlineView;


@end


@implementation TGReportWindowController

- (instancetype)initWithReportGroups:(NSArray *)aGroups variables:(NSDictionary *)aVariables
{
    self = [super initWithWindowNibName:@"TGReportWindow"];
    if (self)
    {
        _reportGroups = aGroups;
        _variables = aVariables;
    }
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.

    // Expand all items
    [self.outlineView expandItem:nil expandChildren:YES];
}

#pragma mark - Getters

- (NSDictionary *)allVariables
{
    // Append the variables we received from our caller with fixed variables for every report
    NSMutableDictionary *lAllVariables = [self.variables mutableCopy];
    [lAllVariables addEntriesFromDictionary:[[self class] fixedVariables]];
    
    return [lAllVariables copy]; // Immutable
}

#pragma mark - Setters

- (void)setCurrentReport:(TGReport *)aReport
{
    _currentReport = aReport;

    self.enablePrint = (aReport != nil);    // Enables/disables print button
}

#pragma mark - Interface actions

- (IBAction)onPrintReport:(id)sender
{
    if (self.currentReport)
    {
        if (!_reportWebview)
        {
            _reportWebview = [[WebView alloc] init];
            _reportWebview.frameLoadDelegate = self;
            _reportWebview.resourceLoadDelegate = self;
            _reportWebview.preferences.usesPageCache = NO;
        }
        
        self.printInfo = self.currentReport.printInfo;  // Cache the printInfo from the selected report
        
        TGLog(@"VARIABLES FOR REPORT: \n%@", self.allVariables);
        
        // Show the progress
        [self.progressBar startAnimation:nil];
        
        // Parse the report
        NSError *lError;
        NSString *lHTMLString = [self.currentReport parsedHTMLWithVariables:self.allVariables error:&lError];
        if (lHTMLString)
        {
            // Render the report off screen
            [self.reportWebview.mainFrame loadHTMLString:lHTMLString
                                                 baseURL:[self.currentReport.reportURL URLByDeletingLastPathComponent]];
            // Continues in webView delegate methods
        }
        else
        {
            // Parsing failed, present the error
            [NSApp presentError:lError];
        }
    } // End if report
}

- (IBAction)onCancelReport:(id)sender
{
    if (self.currentReport)
    {
        [self.currentReport onCancelParsing];
    }
}

- (IBAction)onClose:(id)sender
{
    [NSApp endSheet:[self window]];
	[[self window] orderOut:sender];
}


#pragma mark - Webview Delegate actions

- (void)webView:(WebView *)aWebView didFinishLoadForFrame:(WebFrame *)aFrame
{
    // Update GUI
    [self.progressBar stopAnimation:nil];
    self.progressBar.doubleValue = 0;
    
    // And print the off screen webView
    [self.reportWebview.preferences setShouldPrintBackgrounds:YES];
    
    NSPrintInfo *lPrintInfo = self.printInfo; // The printInfo from the just rendered report
    
    // Set the fixed values for printInfo
    [lPrintInfo setHorizontalPagination:NSFitPagination];
    [lPrintInfo setVerticalPagination: NSAutoPagination];
    [lPrintInfo setVerticallyCentered:NO];
    [lPrintInfo setHorizontallyCentered:NO];
    
    self.reportWebview.frame = NSMakeRect(0, 0,
                                          lPrintInfo.paperSize.width - lPrintInfo.leftMargin - lPrintInfo.rightMargin,
                                          lPrintInfo.paperSize.height - lPrintInfo.topMargin - lPrintInfo.bottomMargin);
    
    // WebView is loaded, then print
    NSView *lView = [[[aWebView mainFrame] frameView] documentView];
    
    NSAssert(lView, @"Expected a view to print in %s", __PRETTY_FUNCTION__);
    NSAssert(lPrintInfo, @"Expected a printInfo for printoperation in %s", __PRETTY_FUNCTION__);
    NSPrintOperation *lOperation = [NSPrintOperation printOperationWithView:lView printInfo:lPrintInfo];
    [lOperation runOperationModalForWindow:self.window
                                  delegate:self
                            didRunSelector:nil
                               contextInfo:nil];
}
/*
- (void)onGeneratePDF
{
    // TODO: future feature: print report to briefpapier
    PDFDocument *lPDFDocument = [[PDFDocument alloc] initWithURL:[NSURL URLWithString:@"/Users/dereuzen/Documents/BriefpapierOVX.pdf"]];

}
*/

#pragma mark - NSOutlineView Datasource

- (BOOL)outlineView:(NSOutlineView *)outlineView isGroupItem:(id)item
{
    return ([item isKindOfClass:[TGReportGroup class]]);
}

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
    NSInteger rv = 0;
    if (!item)
    {
        // Root, return the number of groups
        rv = [self.reportGroups count];
    }
    else if ([item isKindOfClass:[TGReportGroup class]])
    {
        return [[item reports] count];
    }
    return rv;
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item;
{
    id rv;
    if (!item)
    {
        // Root, return the groups
        rv = [self.reportGroups objectAtIndex:index];
    }
    else if ([item isKindOfClass:[TGReportGroup class]])
    {
        rv = [[item reports] objectAtIndex:index];
    }
    return rv;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
    BOOL rv = NO;
    if ([item isKindOfClass:[TGReportGroup class]])
    {
        rv = YES;
    }
    return rv;
}

- (id)outlineView:(NSOutlineView *)aOutlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)aItem
{
    id rv;
    if ([aItem isKindOfClass:[TGReportGroup class]])
    {
        rv = aItem;
    }
    else if ([aItem isKindOfClass:[TGReport class]])
    {
        rv = aItem;
    }
    return rv;
}

#pragma mark - NSOutlineView Delegate

- (NSView *)outlineView:(NSOutlineView *)outlineView viewForTableColumn:(NSTableColumn *)tableColumn item:(id)aItem
{
    // The cells are setup in IB. The textField and imageView outlets are properly setup.
    
    NSTableCellView *rv;
    if ([aItem isKindOfClass:[TGReportGroup class]])
    {
        rv = [outlineView makeViewWithIdentifier:@"HeaderCell" owner:outlineView];
    }
    else
    {
        rv = [outlineView makeViewWithIdentifier:@"DataCell" owner:outlineView];
    }
    return rv;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item
{
    BOOL rv = NO;
    if ([item isKindOfClass:[TGReport class]])
    {
        //allow to be selected
        return YES;
    }
    return rv;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldEditTableColumn:(NSTableColumn *)tableColumn item:(id)item
{
    // Never
    return NO;
}

-(void)outlineViewSelectionDidChange:(NSNotification *)aNotification
{
    if (aNotification.object == self.outlineView)
    {
        id lSelectedItem = [self.outlineView itemAtRow:self.outlineView.selectedRow];
        if ([lSelectedItem isKindOfClass:[TGReport class]])
        {
            self.currentReport = lSelectedItem;
        }
    }
}

#pragma mark - Framework, customizable class methods

+ (NSDictionary *)fixedVariables
{
    // Define the variables you want to feed to _all_ reports
    NSMutableDictionary *lFixedVariables = [NSMutableDictionary new];
    
    // Add current date, get necessary date components
    NSCalendar* lCalendar = [NSCalendar currentCalendar];
    NSDateComponents* lComponents = [lCalendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                                 fromDate:[NSDate date]];
    
    [lFixedVariables addEntriesFromDictionary: @{@"currentDateYear" : @(lComponents.year),
                                                 @"currentDateMonth" : @(lComponents.month),
                                                 @"currentDateDay" : @(lComponents.day),
                                                 @"currentDate" : [NSDate date]}];
    
    // Add currentUser as dict
    TGUser *lUser = [[TGAppDelegate appDelegate] currentUser];
    NSDictionary *userDict = [lUser dictionaryWithValuesForKeys:lUser.allDataEntityProperties.allObjects];
    [lFixedVariables addEntriesFromDictionary:userDict];
    
    return [lFixedVariables copy]; // Immutable
}

@end
