//
//  TGReport+Parsing.m
//  OverviewX
//
//  Created by Marius de Reus on 05/03/15.
//  Copyright (c) 2015 teamGiants. All rights reserved.
//

#import "TGReport+Parsing.h"

// === Application specific imports ===
#import "TGDataTable.h"
#import "TGAppDelegate.h"
#import "TGSettings.h"
// ===

static NSString * const TGReportEngineParsingDidFinish = @"TGReportEngineParsingDidFinish";
static NSString * const TGReportEngineParsingDidCancel = @"TGReportEngineParsingDidCancel";

static NSString * const cSubReportXPathQuery = @"//div[@class='subReport']";
static NSString * const cVariableNodesXPathQuery = @"//div[@class='variable']";
static NSString * const cFilterNodesXPathQuery = @"//div[@class='filter']";
// Query for div nodes with dbQuery just one level deep... '//div' gives all nodes (nested too), 'div' gives only root nodes
static NSString * const cTopLevelQueryXPathQuery = @"/html/body/div[@class='dbQuery']";
static NSString * const cQueryXPathQuery = @"./div[@class='dbQuery']";
static NSString * const cRecordXPathQuery = @".//div[@class='dbRecord']";

static NSString * const cQueryAttributeName = @"query";
static NSString * const cIsMainQueryAttributeName = @"isMainQuery";
static NSString * const cNameAttributeName = @"name";

NSNumberFormatter * gCurrencyNumberFormatter;


@implementation TGReport (Parsing)

@dynamic progress;
@dynamic cancelledByUser;

- (void)onCancelParsing
{
    self.cancelledByUser = YES;
}

- (BOOL)cancelledByUser
{
    return _cancelledByUser;
}

- (void)setProgress:(NSNumber *)aProgress
{
    _progress = aProgress;
}

- (NSXMLDocument*)baseReport:(NSError **)outError
{
    // Step 1: Load the base report as XMLDocument
    NSXMLDocument *rv;
    
    rv = [[NSXMLDocument alloc] initWithContentsOfURL:self.reportURL options:0 error:outError];
    
    if (!rv)
    {
        NSLog(@"Error loading: %@", *outError);
    }
    return rv;
}

- (NSXMLDocument *)reportIncludingSubReports:(NSError **)outError
{
    // Step 2: Replace the subReport nodes in the baseReport by the actual subreports
    NSXMLDocument *lBaseReport = [self baseReport:outError];
    
    // Init mutableString which will contain the parsed report
    NSMutableString *lXMLString = [NSMutableString stringWithFormat:@"%@", lBaseReport];
    
    // Query for subreport nodes, gives all nodes, nested included
    NSArray *lSubReportNodes = [lBaseReport nodesForXPath:cSubReportXPathQuery error:outError];
    
    // Iterate variables
    for (NSXMLElement *lSubReportElement in lSubReportNodes)
    {
        NSString *lSubReportPath = [[lSubReportElement attributeForName:@"reportPath"] objectValue]; // just the last pathcomponent
        NSURL *lSubReportURL = [self subReportURLforFileName:lSubReportPath]; // returns nil when file doesn't exist on disk
        
        if (lSubReportURL)
        {
            // Only substitute when subreport exists
            NSString *lSubReportContent = [[NSString alloc] initWithContentsOfURL:lSubReportURL
                                                                         encoding:NSUTF8StringEncoding error:outError];
            if (lSubReportContent)
            {
                NSString *lSubstringToReplace = [NSString stringWithFormat:@"%@", lSubReportElement];
                
                [lXMLString replaceOccurrencesOfString:lSubstringToReplace
                                            withString:lSubReportContent
                                               options:0
                                                 range:NSMakeRange(0, [lXMLString length])];
            }
            else
            {
                NSLog(@"Error loading subReport: %@", *outError);    // Log, also in release version
            }
        }
    }
    NSXMLDocument *rv = [[NSXMLDocument alloc] initWithXMLString:lXMLString options:0 error:outError];
    if (!rv)
    {
        NSLog(@"Error including subReports: %@", *outError);
    }
    return rv;
}

- (NSXMLDocument *)XMLReportWithParsedVariables:(NSError **)outError
{
    // Step 3: Parse the variables in the report by the values given in the variables dictionary
    if (self.cancelledByUser)
    {
        // Shortcut, when user cancelled the report
        return nil;
    }
    
    NSXMLDocument *lReportIncludingSubReports = [self reportIncludingSubReports:outError];
    
    // Query for variable div nodes. Results in array with all nodes, nested included
    NSArray *lVariableNodes = [lReportIncludingSubReports nodesForXPath:cVariableNodesXPathQuery error:outError];
    if (!lVariableNodes && outError)
    {
        NSLog(@"Error querying variable nodes: %@", *outError);    // Log, also in release version
    }
    
    // Init mutableString which will contain the parsed report
    NSMutableString *lXMLString = [NSMutableString stringWithFormat:@"%@", lReportIncludingSubReports];
    
    // Iterate found variables, and replace the div elements by the variable values
    for (NSXMLElement *lVariableElement in lVariableNodes)
    {
        NSString *lXMLElementAsString = [NSString stringWithFormat:@"%@", lVariableElement];
        NSString *lVariable = [[lVariableElement attributeForName:cNameAttributeName] objectValue];
        
        [lXMLString replaceOccurrencesOfString:lXMLElementAsString
                                    withString:[self stringForReportVariable:lVariable]
                                       options:0
                                         range:NSMakeRange(0, [lXMLString length])];
    }
    
    // Query for filter div nodes. Results in all nodes, nested included
    NSArray *lFilterNodes = [lReportIncludingSubReports nodesForXPath:cFilterNodesXPathQuery error:outError];
    if (!lFilterNodes && outError)
    {
        NSLog(@"Error querying filter nodes : %@", *outError);    // Log, also in release version
    }
    
    // Iterate found filter nodes
//    for (NSXMLElement *lFilterElement in lFilterNodes)
//    {
//        NSString *lFilterName = [[lFilterElement attributeForName:cNameAttributeName] objectValue];
//        NSString *lValue = self.filters[lFilterName];
//        
//        // If value for filter is provided, replace the xml node with the value
//        if (lValue)
//        {
//            NSString *lSubstringToReplace = [NSString stringWithFormat:@"%@", lFilterElement];
//            [lXMLString replaceOccurrencesOfString:lSubstringToReplace
//                                        withString:lValue
//                                           options:0
//                                             range:NSMakeRange(0, [lXMLString length])];
//        }
//    }
    
    // Create XMLDocument from parsed string
    NSXMLDocument *rv = [[NSXMLDocument alloc] initWithXMLString:lXMLString options:0 error:outError];
    if (!rv && outError)
    {
        NSLog(@"Error: %@", *outError);    // Log, also in release version
    }
    
    // Return the report, ready for parsing data
    return rv;
}

- (NSString *)parsedHTMLWithVariables:(NSDictionary *)aVariables error:(NSError **)outError
{
    // Step 4: Parse the dbQuery nodes with their dbRecord and dbField nodes
    if (self.cancelledByUser)
    {
        // Shortcut, when user cancelled the report
        return nil;
    }
    _variables = aVariables;
    self.progress = @0;     // We're starting
    
    NSString *rv;
    NSXMLDocument *lReportWithParsedVariables = [self XMLReportWithParsedVariables:outError];
    
    // Init mutableString which will contain the parsed report
    NSMutableString *lParsedHTMLReport = [NSMutableString stringWithFormat:@"%@",lReportWithParsedVariables];
    
    
    NSArray *lQueryNodes = [lReportWithParsedVariables nodesForXPath:cTopLevelQueryXPathQuery error:outError];
    if (!lQueryNodes)
    {
        NSLog(@"No queryNodes found"); // Always log, can be helpful when creating reports
        if (outError)
        {
            NSLog(@"Error: querying dbQuery nodes: %@", *outError);
        }
    }
    
    // Iterate found query nodes to fill them with data
    for (NSXMLElement *lQueryXMLNode in lQueryNodes)
    {
        if (self.cancelledByUser)
        {
            return nil;
        }
        NSString *lQueryXMLNodeAsString = [NSString stringWithFormat:@"%@", lQueryXMLNode];
        NSString *lTextToReplace = [NSString stringWithFormat:@"%@", [self parseQueryElement:lQueryXMLNode withRecord:nil]];
        
        // Replace the query XMLElement (from start element to end element) with the executed query
        if (lTextToReplace)
        {
            NSUInteger lResult = [lParsedHTMLReport replaceOccurrencesOfString:lQueryXMLNodeAsString
                                                                    withString:lTextToReplace
                                                                       options:NSLiteralSearch
                                                                         range:NSMakeRange(0, [lParsedHTMLReport length]) ];
            if (lResult == 0)
            {
                NSLog(@"No replacements in parsed queryNodes");
            }
        }
    }
    if (self.cancelledByUser)
    {
        rv = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:TGReportEngineParsingDidCancel object:self];
    }
    else
    {
        rv = [lParsedHTMLReport copy]; // Immutable copy
        // We're ready!
        self.progress = @100;
        [[NSNotificationCenter defaultCenter] postNotificationName:TGReportEngineParsingDidFinish object:self];
    }
    return rv;
}


- (NSString *)parseQueryElement:(NSXMLElement *)aQueryXMLElement withRecord:(id)aRecord
{
    // aRecord dict is the parent record when we are parsing a subquery. It is nil for the toplevel queries.
    NSString *rv;
    NSMutableString *lQueryElementAsString = [NSMutableString stringWithFormat:@"%@", aQueryXMLElement];
    NSError *lError;
    NSString *lQuery = [[aQueryXMLElement attributeForName:cQueryAttributeName] objectValue];
    // There should be one main query in a report. The progress is counted against the processing of this query. Typically
    // you use the most important parent query as mainQuery.
    // Don't get the value of this attribute. When we have it, we're the mainQuery
    BOOL lIsMainQuery = ([aQueryXMLElement attributeForName:cIsMainQueryAttributeName]);
    
    // Fill the variables in the query definition. aRecord can be nil.
    NSString *lPreparedQuery = [self preparedQueryFromQuery:lQuery withRecord:aRecord];
    
    // Query database
    NSArray *lDataTable = [[self class] dataForQuery:lPreparedQuery];
    
    if ([lDataTable count] == 0)
    {
        NSLog(@"Possible error: no data for query: %@", lPreparedQuery);  // Log to help evaluate reports
    }
    
    // Find record node within the query node
    NSArray *lRecordElements = [aQueryXMLElement nodesForXPath:cRecordXPathQuery error:&lError];
    
    if ([lRecordElements count] > 0)
    {
        NSXMLElement *lRecordElement = lRecordElements.firstObject;   // There should be only one per dbQuery
        
        int x = 0;
        NSMutableString *lParsedRecords = [NSMutableString string];
        
        // Step through table and add parsed records
        for (id lRecord in lDataTable)
        {
            if (self.cancelledByUser)
            {
                // Shortcut
                return nil;
            }
            
            // Parse lRecord and append it to our parsed records to build the data
            [lParsedRecords appendString:[self parseRecordElement:lRecordElement withRecord:lRecord]];
            
            // If main query, update progress
            if (lIsMainQuery)
            {
                x++;
                [self willChangeValueForKey:@"progress"];
                self.progress = @((double)x / (double)[lDataTable count] * 100.0f);
                [self didChangeValueForKey:@"progress"];
            }
        }
        // Replace the dbRecord element from begin to end with the parsed records
        NSString *lRecordElementAsString = [NSString stringWithFormat:@"%@", lRecordElement];
        
        [lQueryElementAsString replaceOccurrencesOfString:lRecordElementAsString
                                               withString:[lParsedRecords copy]  // immutable
                                                  options:NSLiteralSearch
                                                    range:NSMakeRange(0, lQueryElementAsString.length) ];
        
        rv = [lQueryElementAsString copy]; // immutable
    }
    else
    {
        NSLog(@"No recordNodes in element: %@", aQueryXMLElement); // Log to help evaluate reports
    }
    return rv;
}

- (NSString *)parseRecordElement:(NSXMLElement *)aRecordElement withRecord:(id)aRecord
{
    NSError *error;
    
    NSMutableString *rv = [NSMutableString stringWithFormat:@"%@", aRecordElement];
    
    // First parse subqueries in the record
    NSArray *lSubQueries = [aRecordElement nodesForXPath:cQueryXPathQuery error:&error];
    
    for (NSXMLElement *lQuery in lSubQueries)
    {
        // iterate subQueries and replace them in the rawElement
        NSString *lQueryElementAsString = [NSString stringWithFormat:@"%@", lQuery];
        NSString *lQueryResult = [self parseQueryElement:lQuery withRecord:aRecord];
        
        if (lQueryResult)
        {
            [rv replaceOccurrencesOfString:lQueryElementAsString
                                withString:lQueryResult
                                   options:0
                                     range:NSMakeRange(0, rv.length) ];
        }
    }
    
    // find fields in the record
    NSArray *lFields = [aRecordElement nodesForXPath:@"//div[@class='dbField']" error:&error];
    
    for (NSXMLElement *lFieldElement in lFields)
    {
        // iterate fields
        NSString *lFieldElementAsString = [NSString stringWithFormat:@"%@", lFieldElement];
        NSString *lParsedField = [self parseFieldElement:lFieldElement withRecord:aRecord];
        
        if (lParsedField)
        {
            [rv replaceOccurrencesOfString:lFieldElementAsString
                                withString:lParsedField
                                   options:0
                                     range:NSMakeRange(0, rv.length)];
        }
    }
    
    // Return parsed recordElement as string
    return [rv copy];
}

- (NSString *)parseFieldElement:(NSXMLElement *)aXMLElement withRecord:(id)aRecord
{
    NSString *rv = @"";
    
    NSString *lFieldName = [[aXMLElement attributeForName:@"name"] objectValue];
    id lValue = [aRecord valueForKey:lFieldName];
    
    if (lValue && lValue != [NSNull null])
    {
        // Value available, is there a format attribute?
        if ([aXMLElement attributeForName:@"format"])
        {
            NSString *lFormat = [[aXMLElement attributeForName:@"format"] objectValue];
            if ([lFormat isEqualToString:@"currency"])
            {
                // Format as currency
                if (!gCurrencyNumberFormatter)
                {
                    // Lazy instantiation
                    gCurrencyNumberFormatter = [[NSNumberFormatter alloc] init];
                    
                    gCurrencyNumberFormatter.formatterBehavior = NSNumberFormatterBehavior10_4;
                    gCurrencyNumberFormatter.numberStyle = NSNumberFormatterCurrencyStyle;

                }
                rv = [gCurrencyNumberFormatter stringFromNumber:@([lValue doubleValue])];
            }
        }
        else
        {
            // Unformatted result as string. Will return empty string when no value
            rv = [self stringForKey:lFieldName fromRecord:aRecord];
        }
    }
    return rv;
}

- (NSString *)preparedQueryFromQuery:(NSString *)aQuery withRecord:(id)aRecord
{
    // Prepare aQuery for use by replacing variables in the query with values
    
    // aRecord dictionary can contain data from a parent record, but is nil for top level queries
    // e.g. with subquery for receipts in a project, aRecord contains projectId
    NSMutableString *lPreparedQuery = [aQuery mutableCopy];  // Start with the original query
    
    NSArray *lVariablesInQuery = [self variablesInQuery:aQuery];
    
    // Iterate through variables
    for (NSString *lVariable in lVariablesInQuery)
    {
        NSString *lStringValue;
        if (aRecord && [aRecord valueForKey:lVariable] && [aRecord valueForKey:lVariable] != [NSNull null])
        {
            // Use variable from parent record
            lStringValue = [self stringForKey:lVariable fromRecord:aRecord];
        }
        else
        {
            // No value from parent record, use report variables
            //use report variable
            lStringValue = [self stringForReportVariable:lVariable];
        }
        
        if (lStringValue)
        {
            [lPreparedQuery replaceOccurrencesOfString:[NSString stringWithFormat:@"#%@#", lVariable]
                                            withString:lStringValue
                                               options:0
                                                 range:NSMakeRange(0, lPreparedQuery.length)];
        }
        else
        {
            NSLog(@"Possible error: No value for variable: %@", lVariable); // Log, can be helpful when creating reports
        }
    }
    return [lPreparedQuery copy]; // Immutable copy
}

- (NSArray *)variablesInQuery:(NSString *)aQuery
{
    // Scan for variables in query with format #variable#
    NSMutableArray *lVariablesInQuery = [NSMutableArray array];
    NSScanner *lScanner = [NSScanner scannerWithString:aQuery];
    NSString *lDelimiter = @"#";
    
    while (!lScanner.isAtEnd)
    {
        NSString *lVariable;
        
        [lScanner scanUpToString:lDelimiter intoString:nil];
        [lScanner scanString:lDelimiter intoString:nil];        // Excluding the delimiters
        [lScanner scanUpToString:lDelimiter intoString:&lVariable];
        [lScanner scanString:lDelimiter intoString:nil];
        
        if (lVariable)
        {
            [lVariablesInQuery addObject:lVariable];
        }
    }
    return [lVariablesInQuery copy]; // Immutable copy
}

- (NSString *)stringForReportVariable:(NSString *)aVariable
{
    // Retrieve the value for a variable from our variables dictionary. Note that we need a string value for usage in
    // the query definition. So convert the object to a stringValue.
    NSString *rv = [self stringForKey:aVariable fromObject:self.variables];
    if (!rv)
    {
        // No value found for this variable. Return an empty string and log a possible error, this might be helpful
        // when creating reports
        NSLog(@"No value found for report variable %@", aVariable);
        rv = @""; // Return an empty string instead
    }
    return rv;
}

- (NSString *)stringForKey:(NSString *)aKey fromRecord:(id)aRecord
{
    NSString *rv = [self stringForKey:aKey fromObject:aRecord];
    if (!rv)
    {
        // No value found for this variable. Return an empty string and log a possible error, this might be helpful
        // when creating reports
        NSLog(@"No value found for record variable %@", aKey);
        rv = @""; // Return an empty string instead
    }
    return rv;
}

- (NSString *)stringForKey:(NSString *)aKey fromObject:(id)aObject
{
    NSString *rv;
    
    id lValue = [aObject valueForKey:aKey];
    if (lValue)
    {
        // Variable found, we want the string. It might already be a string, therefore be careful and use the
        // respondsToSelector construction here.
        if ([lValue respondsToSelector:@selector(stringValue)])
        {
            rv = [lValue stringValue];
        }
        else
        {
            rv = (NSString *)lValue;
        }
    }
    return rv;
}

- (NSURL *)subReportURLforFileName:(NSString*)aFileName
{
    NSURL *rv;
    
    // First search for subreport in the same folder as the current mainReport
    NSURL *lCurrentReportFolderURL = [self.reportURL URLByDeletingLastPathComponent];
    NSString *lProposedCompleteSubReportPath = [[lCurrentReportFolderURL.path stringByAppendingPathComponent:aFileName] stringByAppendingPathExtension:@"html"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:lProposedCompleteSubReportPath])
    {
        rv = [NSURL fileURLWithPath:lProposedCompleteSubReportPath];
    }
    else
    {
        // Not found, search in the applications default main subReports folder
        NSURL *lDefaultSubReportsFolder = [[self class] subReportsFolder];
        NSURL *lSubReportURL = [[lDefaultSubReportsFolder URLByAppendingPathComponent:aFileName] URLByAppendingPathExtension:@"html"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:lSubReportURL.path])
        {
            rv = lSubReportURL;
        }
    }
    return rv;
}

#pragma mark - Implementation specific class methods

+ (NSArray *)dataForQuery:(NSString *)aQuery
{
    // Note: Replace this implementation with the application specific data query implementation
    NSArray *rv;
    if (aQuery)
    {
        TGDataTable *lTable = [[TGDataTable alloc] initWithEntityClass:[NSMutableDictionary class] query:aQuery];
        rv = lTable.dataArray;
    }
    return rv;
}

+ (NSURL *)topLevelReportFolder
{
    // The top level folder of the report folders on disk
    NSURL *rv;
    NSString *lPath = [TGAppDelegate appDelegate].currentSettings.settingsReportMainPath;
    if (lPath)
    {
        rv = [NSURL fileURLWithPath:lPath];
    }
    return rv;
}

+ (NSURL *)subReportsFolder
{
    // The default folder for subReports (when not siblings in the baseReport folder)
    NSString * const cSubReportPath = @"SubReports";
    return [[[self class] topLevelReportFolder] URLByAppendingPathComponent:cSubReportPath];
}


@end
