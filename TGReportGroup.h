//
//  TGReportGroup.h
//  OverviewX
//
//  Created by Marius de Reus on 16/03/15.
//  Copyright (c) 2015 teamGiants. All rights reserved.
//

/*
 * A reportgroup represents a folder on disk, containing TGReport files
 *
 */


#import <Foundation/Foundation.h>

@interface TGReportGroup : NSObject

- (instancetype)initWithRelativePath:(NSString *)aPath name:(NSString *)aName NS_DESIGNATED_INITIALIZER;

@property (readonly, nonatomic) NSArray *reports;

@end
