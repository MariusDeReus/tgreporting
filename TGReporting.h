//
//  TGReporting.h
//  OverviewX
//
//  Created by Marius de Reus on 16/03/15.
//  Copyright (c) 2015 teamGiants. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const cReportErrorDomain;
typedef NS_ENUM(NSInteger, TGReportError)
{
    // ErrorCodes for reportErrorDomain
    cReportErrorMainFolderNotFound,
    cReportErrorMainFolderNotDefined
};


@interface TGReporting : NSObject

@end
