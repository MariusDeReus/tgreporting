//
//  TGReport.h
//  OverviewX
//
//  Created by Marius de Reus on 02/03/15.
//  Copyright (c) 2015 teamGiants. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TGReport : NSObject
{
    NSDictionary *_variables; // Public for parsing category
    
    NSNumber *_progress;
    BOOL _cancelledByUser;
}

// Designated initializer. Variables and filters are optional arguments.
- (instancetype)initWithReportURL:(NSURL*)aReportURL NS_DESIGNATED_INITIALIZER;

@property (readonly) NSURL *reportURL;
@property (readonly) NSString *name;                // Last pathcomponent of our URL
@property (readonly) NSString *reportDescription;   // Description in the <head> section of the report


@property (readonly) NSPrintInfo *printInfo;        // Print info properties
@property (readonly) NSDictionary *variables;       // Public for Parsing category
@property (readonly, nonatomic) NSArray *filters;   // Array of TGReportFilter objects

// Parsing
@property (readonly) NSNumber *progress;            // Progress parsing the mainQuery

@end
