//
//  TGReporting.m
//  OverviewX
//
//  Created by Marius de Reus on 16/03/15.
//  Copyright (c) 2015 teamGiants. All rights reserved.
//

#import "TGReporting.h"

NSString * const cReportErrorDomain = @"com.teamGiants.OverviewX.reporting";

@implementation TGReporting

@end
